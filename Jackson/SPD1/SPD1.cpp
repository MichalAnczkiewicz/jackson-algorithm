

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
using namespace std;

//int numberOfOperations;

class Task {
public:
	int r;
	int p;
	int nr;
};

Task arrayTasks[100000];
Task numberOfTasks;
bool readFile(string fileName) {
	int i = 0;
	ifstream file;
	file.open(fileName.c_str());
	if (!file.good()) {
		return false;
	}

	while (i < numberOfTasks.nr + 1) {
		if (i == 0) {
			file >> numberOfTasks.nr;
			//cout << numberOfOperations;
		}
		else {
			file >> arrayTasks[i].r >> arrayTasks[i].p;
		}
		i++;
	}
	file.close();
	return true;
}

void printArray() {
	for (int i = 1; i < numberOfTasks.nr + 1; i++) {
		cout << arrayTasks[i].r << " " << arrayTasks[i].p << endl;
	}
}

void sortTasks() {
	int temp = numberOfTasks.nr;
	Task task;
	while (temp > 1) {
		for (int i = 1; i < temp; i++) {
			if (arrayTasks[i].r > arrayTasks[i + 1].r) {
				task = arrayTasks[i];
				arrayTasks[i] = arrayTasks[i + 1];
				arrayTasks[i + 1] = task;
			}
		}

		temp -= 1;
	}

}


int main(){

	int Cmax[100000];
	Cmax[0] = 0;

	if (!readFile("JACK4.DAT"))
		cout << "Error while opening file" << endl;
	sortTasks();
	printArray();

	for (int i = 1; i <= numberOfTasks.nr; i++) {
		Cmax[i] = max(Cmax[i - 1], arrayTasks[i].r) + arrayTasks[i].p;
	}

	cout << "\nCmax = " << Cmax[numberOfTasks.nr] << endl;

	getchar();
	getchar();
    return 0;
}

